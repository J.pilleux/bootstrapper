{-# LANGUAGE OverloadedStrings #-}

module Main where

import CommandController
  ( getAllCommandStr,
    getAllInstalledCommands,
    parseCmdList,
    runDescribe,
    runInstallCommand,
    runUninstallCommand,
  )
import Commands.Errors (Error (Error))
import Data.Maybe (fromJust, isJust)
import ParseArgs
import System.Console.GetOpt (usageInfo)
import Utils.Format (formatError, formatErrorOrSuccess, formatSimpleList, formatTitle)
import Data.Foldable (for_)

spacing :: String
spacing = "    "

printHelp :: IO ()
printHelp = do
  putStrLn $ usageInfo "bootstrapper [OPTIONS]\n" options

runCmd :: (String -> IO (Either Error String)) -> String -> IO ()
runCmd f c = do
  res <- f c
  putStrLn $ formatErrorOrSuccess res

optParser :: Options -> IO ()
optParser (Options help list run undo desc mult listInstalled)
  | help = printHelp
  | list = do
    putStrLn $ formatTitle "Available Commands"
    putStrLn ""
    putStrLn $ formatSimpleList getAllCommandStr
  | listInstalled = do
    putStrLn $ formatTitle "Installed"
    putStrLn ""
    putStrLn $ formatSimpleList getAllInstalledCommands
  | isJust run && isJust undo = do
    putStrLn "Cannot use -r and -u at the same time"
    printHelp
  | isJust run = do
    putStrLn $ "Installing command " ++ fromJust run
    runCmd runInstallCommand $ fromJust run
  | isJust run = do
    putStrLn $ "Uninstalling command " ++ fromJust undo
    runCmd runUninstallCommand $ fromJust undo
  | isJust mult = do
    resList <- parseCmdList $ fromJust mult
    for_ (map formatErrorOrSuccess resList) putStrLn
  | isJust desc = do
    case runDescribe $ fromJust desc of
      Left (Error err) -> putStrLn $ formatError err
      Right str -> putStrLn str
  | otherwise = printHelp

main :: IO ()
main = do
  opt <- getOptions
  optParser opt
