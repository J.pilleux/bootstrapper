BIN = "/home/julien/files/projects/bootstrapper/dist-newstyle/build/x86_64-linux/ghc-8.10.7/bootstrapper-0.1.0.0/x/bootstrapper/build/bootstrapper/bootstrapper"
CMD = ""

all: build

build:
	cabal build

debug:
	$(BIN)

run:
	$(BIN) -r $(CMD)

undo:
	$(BIN) -u $(CMD)

list:
	$(BIN) -l

installed:
	$(BIN) -i

multi:
	$(BIN) -m $(CMD)

help:
	$(BIN) -h

deploy:
	cp $(BIN) ~/.dotfiles/usr_bin/.local/bin/bootstrapper
