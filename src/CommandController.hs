{-# LANGUAGE ExistentialQuantification #-}

module CommandController where

import Commands.AlacrittyUbuntu (alacrittyUbuntu)
import Commands.Base (base)
import Commands.Command
import Commands.Errors (Error (Error))
import Commands.Ghcup (ghcup)
import Commands.Nvim (nvim)
import Commands.Rustup (rustup)
import Commands.Stowing (stowing)
import Commands.Yay (yay)

installAnyCommand :: AnyCommand -> IO (Either Error String)
installAnyCommand (AnyCommand c) = install c

undoAnyCommand :: AnyCommand -> IO (Either Error String)
undoAnyCommand (AnyCommand c) = undo c

describeCommand :: AnyCommand -> String
describeCommand (AnyCommand c) = show $ describe c

getAllCommands :: [AnyCommand]
getAllCommands =
  [ nvim,
    yay,
    ghcup,
    base,
    alacrittyUbuntu,
    rustup,
    stowing
  ]

getAllCommandStr :: [String]
getAllCommandStr = map (\(AnyCommand c) -> getName c) getAllCommands

getAllInstalledCommands :: [String]
getAllInstalledCommands = map (\(AnyCommand c) -> getName c) installedCommands
  where
    installedCommands = filter getIsInstalled getAllCommands

parseCmdList :: String -> IO [Either Error String]
parseCmdList cmds = mapM runInstallCommand (words cmds)

getCmd :: String -> Either Error AnyCommand
getCmd "nvim" = Right nvim
getCmd "yay" = Right yay
getCmd "ghcup" = Right ghcup
getCmd "base" = Right base
getCmd "rustup" = Right rustup
getCmd "alacrittyUbuntu" = Right alacrittyUbuntu
getCmd "stow" = Right stowing
getCmd c = Left $ Error $ "Unknown command " ++ c

runCommand :: AnyCommand -> IO (Either Error String)
runCommand (AnyCommand c) = install c

runInstallCommand :: String -> IO (Either Error String)
runInstallCommand cmd =
  case getCmd cmd of
    Left err -> return $ Left err
    Right c -> installAnyCommand c

runUninstallCommand :: String -> IO (Either Error String)
runUninstallCommand cmd =
  case getCmd cmd of
    Left err -> return $ Left err
    Right c -> undoAnyCommand c

runDescribe :: String -> Either Error String
runDescribe cmd =
  case getCmd cmd of
    Left err -> Left err
    Right c -> Right $ describeCommand c
