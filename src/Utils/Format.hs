module Utils.Format where

import System.Console.Pretty (Color (..), color)
import Commands.Errors (Error(Error))
import Data.List (intercalate)

maxLen :: Int
maxLen = 40

fillChar :: [Char]
fillChar = "_"

getFiller :: Int -> String
getFiller len = concat $ replicate ((maxLen `div` 2) - len) fillChar

formatTitle :: String -> String
formatTitle t = filler ++ " " ++ title ++ " " ++ filler
  where
    filler = color Cyan $ getFiller $ length t
    title = color Yellow t

formatParam :: Int -> (String, String) -> String
formatParam maxTextSize (d, value) = color Green d ++ pad ++ value
  where
    pad = concat $ replicate padSize " "
      where
        padSize = (maxTextSize - length d) + 1

formatList :: [(String, String)] -> String
formatList list = intercalate "\n" $ map (color Yellow "    > " ++) l
  where
    l = map (formatParam m) list
      where
        m = maximum $ map (length . fst) list

formatSimpleList :: [String] -> String
formatSimpleList list = intercalate "\n" $ map (color Yellow "    > " ++) list

formatErrorOrSuccess :: Either Error String -> String
formatErrorOrSuccess msg =
  case msg of
    Left (Error err) -> formatError err
    Right str -> formatSuccess str

formatError :: String -> String
formatError err = color Red "Error" ++ ": " ++ err

formatSuccess :: String -> String
formatSuccess suc = color Green "Success" ++ ": " ++ suc
