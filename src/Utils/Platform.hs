module Utils.Platform where
import System.Posix (fileExist, getEnv)
import GHC.IO (unsafePerformIO)
import Commands.Errors (Error (Error))
import System.Exit
import Control.Monad.IO.Class (MonadIO (liftIO))
import qualified System.Directory as Directory
import System.Process (callCommand)

data Platform = Debian | Arch

aptPath :: String
aptPath = "/usr/bin/apt"

pacmanPath :: String
pacmanPath = "/usr/bin/pacman"

archError :: Error
archError = Error "This can only by installed on Arch based GNU/Linux distributions"

cheater :: String -> Bool
cheater = unsafePerformIO . fileExist

getPlatform :: Either Error Platform
getPlatform
  | cheater aptPath = Right Debian
  | cheater pacmanPath = Right Arch
  | otherwise = Left $ Error "Unknown platform"

isArch :: IO Bool
isArch = fileExist pacmanPath

getHome :: IO String
getHome = do
  home <- getEnv "HOME"
  case home of
    Nothing -> die "Cannot get HOME environment variable"
    Just h -> return h

cd :: MonadIO io => FilePath -> io ()
cd path = liftIO (Directory.setCurrentDirectory path)

stow :: String -> IO ()
stow packages = callCommand $ "stow " ++ packages
