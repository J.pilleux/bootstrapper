module Commands.Stowing (stowing) where

import Commands.Command
  ( AnyCommand (AnyCommand),
    Command (..),
    CommandDescription (..),
  )
import GHC.IO (unsafePerformIO)
import Utils.Platform (cd, getHome, stow)

data Stowing = Stowing
  { packages :: String,
    unsafePackages :: String,
    location :: String
  }

instance Command Stowing where
  getName _ = "stow"

  install (Stowing p u l) = do
    cd l
    stow p
    stow $ "--adopt " ++ u
    return $ Right "Stow packages installed"

  undo (Stowing p u l) = do
    cd l
    stow $ "-D " ++ p ++ " " ++ u
    return $ Right "Stow packages uninstalled"

  isInstalled _ = return True

  describe (Stowing p u l) =
    CommandDescription
      { title = "Stowing",
        desc = "Installs stow packages",
        params =
          [ ("Stow packages", p),
            ("Unsafe packages", u),
            ("Stow location", l)
          ]
      }

stowing :: AnyCommand
stowing =
  AnyCommand
    Stowing
      { packages = "git less rofi usr_bin alacritty fonts i3 nvim tmux wallpapers",
        unsafePackages = "bash",
        location = unsafePerformIO getHome ++ "/.dotfiles/"
      }
