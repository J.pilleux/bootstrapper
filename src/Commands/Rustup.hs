module Commands.Rustup (rustup) where

import Commands.Command
  ( AnyCommand (AnyCommand),
    Command (..),
    CommandDescription (..),
  )
import System.Posix (fileExist)
import System.Process

data Rustup = Rustup
  { bin :: String,
    installCmd :: String
  }

instance Command Rustup where
  getName _ = "rustup"

  install (Rustup _ c) = do
    callCommand c
    return $ Right "rustup installed"

  undo _ = undefined

  isInstalled (Rustup b _) = fileExist b

  describe (Rustup b cmd) =
    CommandDescription
      { title = "Rustup",
        desc = "Installs Rustup, a tool to make Haskell development easeier",
        params =
          [ ("Binary location", b),
            ("Installation command", cmd)
          ]
      }

rustup :: AnyCommand
rustup =
  AnyCommand
    Rustup
      { bin = "/home/julien/.cargo/bin/rustup",
        installCmd = "curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh"
      }
