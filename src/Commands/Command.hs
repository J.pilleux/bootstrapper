{-# LANGUAGE GADTs #-}

module Commands.Command
  ( Command (..),
    CommandDescription (..),
    AnyCommand (..),
    getIsInstalled,
  )
where

import GHC.IO (unsafePerformIO)
import Utils.Format (formatTitle, formatList)
import Commands.Errors

data CommandDescription = CommandDescription
  { title :: String,
    desc :: String,
    params :: [(String, String)]
  }

data AnyCommand where
  AnyCommand :: Command c => c -> AnyCommand

getIsInstalled :: AnyCommand -> Bool
getIsInstalled (AnyCommand c) = unsafePerformIO $ isInstalled c

instance Show CommandDescription where
  show (CommandDescription t d p) =
    formatTitle t
      ++ "\n\n"
      ++ d
      ++ "\n\n"
      ++ formatList p

class Command a where
  getName :: a -> String
  install :: a -> IO (Either Error String)
  undo :: a -> IO (Either Error String)
  isInstalled :: a -> IO Bool
  describe :: a -> CommandDescription

