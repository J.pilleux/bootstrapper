module Commands.AlacrittyUbuntu where

import Commands.Command (AnyCommand (AnyCommand), Command (..), CommandDescription (..))
import System.Posix.Files (fileExist)
import System.Process (callCommand)

data AlacrittyUbuntu = AlacrittyUbuntu

instance Command AlacrittyUbuntu where
  getName _ = "alacrittyUbuntu"
  install _ = do
    callCommand "apt update"
    callCommand "add-apt-repository ppa:aslatter/ppa"
    callCommand "apt install alacritty"
    return $ Right "Alacritty installed"

  undo _ = do
    callCommand "apt purge alacritty"
    return $ Right "Alacritty uninstalled"

  isInstalled _ = fileExist "/usr/bin/alacritty"

  describe _ =
    CommandDescription
      { title = "Alacritty Ubuntu",
        desc = "Installs the terminal Alacritty (works only on Ubuntu)",
        params = []
      }

alacrittyUbuntu :: AnyCommand
alacrittyUbuntu =
  AnyCommand
    AlacrittyUbuntu
