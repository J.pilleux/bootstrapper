module Commands.Ghcup (ghcup) where

import Commands.Command
  ( AnyCommand (AnyCommand),
    Command (..),
    CommandDescription (..),
  )
import System.Posix (fileExist)
import System.Process

data Ghcup = Ghcup
  { bin :: String,
    installCmd :: String,
    delCmd :: String
  }

instance Command Ghcup where
  getName _ = "ghcup"

  install (Ghcup _ c _) = do
    callCommand c
    return $ Right "ghcup installed"

  undo (Ghcup _ _ d) = do
    callCommand d
    return $ Right "ghcup uninstalled"

  isInstalled (Ghcup b _ _) = fileExist b

  describe (Ghcup b cmd d) =
    CommandDescription
      { title = "Ghcup",
        desc = "Installs Ghcup, a tool to make Haskell development easeier",
        params =
          [ ("Binary location", b),
            ("Installation command", cmd),
            ("Uninstallation command", d)
          ]
      }

ghcup :: AnyCommand
ghcup =
  AnyCommand
    Ghcup
      { bin = "/home/julien/.ghcup/bin/ghcup",
        installCmd = "curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh",
        delCmd = "ghcup nuke"
      }
