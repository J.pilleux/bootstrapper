{-# LANGUAGE OverloadedStrings #-}

module Commands.Nvim (nvim) where

import Commands.Command
import qualified Data.ByteString.Lazy as BS (writeFile)
import Network.HTTP.Conduit
import System.Directory
  ( createDirectoryIfMissing,
    removeDirectory,
    removeFile,
  )
import System.Posix.Files
  ( createSymbolicLink,
    fileExist,
    setFileMode,
  )

data Nvim = Nvim
  { prefix :: String,
    url :: String,
    appimage :: String,
    bin :: String
  }

instance Command Nvim where
  getName _ = "nvim"

  install (Nvim p u a b) = do
    createDirectoryIfMissing True p
    binFile <- simpleHttp u
    let binPath = p ++ a
    BS.writeFile binPath binFile
    setFileMode binPath 33261
    createSymbolicLink binPath b
    return $ Right $ "NeoVim successfuly installed in " ++ p

  undo (Nvim p _ _ b) = do
    removeFile b
    removeDirectory p
    return $ Right "NeoVim uninstalled !"

  isInstalled (Nvim p _ a _) = fileExist $ p ++ a

  describe (Nvim p u _ b) =
    CommandDescription
      { title = "NeoVim",
        desc = "Installs the nighly version of NeoVim",
        params =
          [ ("Installation location", p),
            ("Git repository URL", u),
            ("Binary location", b)
          ]
      }

nvim :: AnyCommand
nvim =
  AnyCommand
    Nvim
      { prefix = "/opt/nvim/",
        url = "https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage",
        appimage = "nvim.appimage",
        bin = "/usr/local/bin/nvim"
      }
