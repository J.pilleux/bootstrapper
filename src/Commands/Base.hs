module Commands.Base where

import Commands.Command
  ( AnyCommand (AnyCommand),
    Command (..),
    CommandDescription (..),
  )
import Commands.Errors (Error (Error))
import System.Process (callCommand)
import Utils.Platform (Platform (..), getPlatform)

data Base = Base
  { archPackages :: String,
    debianPackages :: String
  }

runInstall :: Base -> Platform -> IO ()
runInstall (Base a _) Arch = callCommand $ "pacman -S " ++ a
runInstall (Base _ d) Debian = do
  callCommand "apt update"
  callCommand $ "apt install " ++ d

base :: AnyCommand
base =
  AnyCommand
    Base
      { archPackages = "base-devel gcc make tmux ripgrep fzf cmake wget i3-wm picom rofi feh nitrogen openssh nodejs i3blocks stow curl alacritty",
        debianPackages = "stow gcc make doas tmux i3 ripgrep fd-find fzf libfuse2 curl wget"
      }

instance Command Base where
  getName _ = "base"

  install b = do
    case getPlatform of
      Left err -> return $ Left err
      Right p -> do
        runInstall b p
        return $ Right "Base packages installed"

  undo _ = return $ Left $ Error "Cannot uninstall"

  isInstalled _ = return True

  describe (Base a d) =
    CommandDescription
      { title = "Base packages",
        desc = "Installs the base packages for the corresponding distribution",
        params =
          [ ("Arch GNU/Linux packges", a),
            ("Debian GNU/Linux packages", d)
          ]
      }
