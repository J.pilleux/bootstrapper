module Commands.Yay (yay) where

import Commands.Command
import Commands.Errors (Error (Error))
import System.Posix (fileExist)
import System.Posix.Env (getEnv)
import System.Process (callCommand)
import Utils.Platform (archError, cd, isArch)

data Yay = Yay
  { prefix :: String,
    gitRepo :: String,
    yayBin :: String
  }

userEnvVar :: String
userEnvVar = "USER"

instance Command Yay where
  getName _ = "yay"

  install (Yay p g _) = do
    correctOs <- isArch
    if not correctOs
      then return $ Left archError
      else do
        callCommand $ "sudo git clone " ++ g ++ " " ++ p
        currUsr <- getEnv userEnvVar
        case currUsr of
          Nothing ->
            return $
              Left $ Error $ "Cannot get environment variable $" ++ userEnvVar
          Just u -> do
            callCommand $ "sudo chown -R " ++ u ++ ":" ++ u ++ " " ++ p
            cd p
            callCommand "makepkg -si"
            return $ Right "Yay installed"

  undo _ = do
    correctOs <- isArch
    if not correctOs
      then return $ Left archError
      else do
        callCommand "sudo pacman -Rs yay"
        return $ Right "Yay uninstalled"

  isInstalled (Yay _ _ y) = fileExist y

  describe (Yay p g _) =
    CommandDescription
      { title = "Yay",
        desc = "Installs Yay, a AUR helper",
        params =
          [ ("Installation location", p),
            ("Git repository address", g)
          ]
      }

yay :: AnyCommand
yay =
  AnyCommand
    Yay
      { prefix = "/opt/yay",
        gitRepo = "https://aur.archlinux.org/yay-git.git",
        yayBin = "/usr/bin/yay"
      }
