module ParseArgs (getOptions, Options (..), options) where

import System.Console.GetOpt
import System.Environment (getArgs)

data Flag
  = Help
  | List Bool
  deriving (Show)

data Options = Options
  { optHelp :: Bool,
    optList :: Bool,
    optRun :: Maybe String,
    optUndo :: Maybe String,
    optDesc :: Maybe String,
    optMultiple :: Maybe String,
    optInstalled :: Bool
  }

startOptions :: Options
startOptions =
  Options
    { optHelp = False,
      optList = False,
      optRun = Nothing,
      optUndo = Nothing,
      optDesc = Nothing,
      optMultiple = Nothing,
      optInstalled = False
    }

options :: [OptDescr (Options -> IO Options)]
options =
  [ Option
      "h"
      ["help"]
      (NoArg (\opt -> return opt {optHelp = True}))
      "Display this message",
    Option
      "l"
      ["list"]
      (NoArg (\opt -> return opt {optList = True}))
      "List all available commands",
    Option
      "r"
      ["run"]
      (ReqArg (\arg opt -> return opt {optRun = Just arg}) "<COMMAND>")
      "Install a given command (Incompatible with -u)",
    Option
      "u"
      ["undo"]
      (ReqArg (\arg opt -> return opt {optUndo = Just arg}) "<COMMAND>")
      "Uninstall a given command (Incompatible with -r)",
    Option
      "d"
      ["describe"]
      (ReqArg (\arg opt -> return opt {optDesc = Just arg}) "<COMMAND>")
      "Print the description of a command",
    Option
      "i"
      ["installed"]
      (NoArg (\opt -> return opt {optInstalled = True}))
      "List all installed commands",
    Option
      "m"
      ["multiple"]
      (ReqArg (\arg opt -> return opt {optMultiple = Just arg}) "<COMMAND_LIST>")
      "Install a list of command. The command should be separated by spaces"
  ]

getOptions :: IO Options
getOptions = do
  args <- getArgs
  let (actions, _, _) = getOpt RequireOrder options args
  foldl (>>=) (return startOptions) actions
