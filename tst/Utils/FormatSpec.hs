module Utils.FormatSpec where

import Test.Hspec
import Utils.Format

spec :: Spec
spec =
  describe "Format" $ do
    describe "getFiller" $ do
      it "Should get a filler of length 10 with 10" $ do
        length (getFiller 10) `shouldBe` 10

      it "Should be null with a length of 20" $ do
        length (getFiller 20) `shouldBe` 0

    describe "formatTitle" $ do
      it "" $ do
        formatTitle "toto" `shouldBe` "\ESC[96m________________\ESC[0m \ESC[93mtoto\ESC[0m \ESC[96m________________\ESC[0m"
